class Book
  attr_reader :title
  @@uncapitalized_words = ['a', 'an', 'and', 'in', 'of', 'the']

  def initialize
    @title = ''
  end

  def title=(value)
    value.capitalize!
    words = []
    value.split(' ').each do |word|
      word.capitalize! unless @@uncapitalized_words.include?(word)
      words << word
    end
    @title = words.join(' ')
  end
end
