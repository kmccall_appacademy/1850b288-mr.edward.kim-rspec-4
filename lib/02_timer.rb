class Timer
  attr_accessor :seconds

  def initialize
    @seconds = 0
  end

  def time_string
    hour = padded(@seconds / 3600)
    minutes = padded(@seconds / 60 % 60)
    seconds = padded(@seconds % 60)
    "#{hour}:#{minutes}:#{seconds}"
  end

  def padded(num)
    num < 10 ? "0#{num}" : num
  end
end
