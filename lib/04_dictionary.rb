class Dictionary
  def initialize
    @entries = {}
  end

  def entries
    @entries
  end

  def add(args)
    if args.is_a? String
      @entries[args] = nil
    elsif args.is_a? Hash
      args.each { |k, v| @entries[k] = v }
    end
  end

  def keywords
    keys = []
    @entries.each_key { |k| keys << k }
    keys.sort!
  end

  def include?(str)
    @entries.key?(str)
  end

  def find(str)
    result = {}
    @entries.keys.select { |k| k.include? str }.each do |key|
      result[key] = @entries[key]
    end
    result
  end

  def printable
    str = ''
    keywords.each do |key|
      str += "\n" unless str == ''
      str += "[#{key}] \"#{@entries[key]}\""
    end
    str
  end
end
