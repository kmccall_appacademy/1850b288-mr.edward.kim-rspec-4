class Temperature
  def initialize(args)
    args.each { |k, v| instance_variable_set "@#{k}", v }
    @f = @c * 9 / 5.0 + 32 if @c
    @c = (@f - 32) * 5 / 9.0 if @f
  end

  def in_fahrenheit
    @f
  end

  def in_celsius
    @c
  end

  def self.from_celsius(temperature)
    Temperature.new(c: temperature)
  end

  def self.from_fahrenheit(temperature)
    Temperature.new(f: temperature)
  end
end

class Celsius < Temperature
  def initialize(temperature)
    super(c: temperature)
  end
end

class Fahrenheit < Temperature
  def initialize(temperature)
    super(f: temperature)
  end
end
